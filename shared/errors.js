module.exports = {
  JOB: {
    LOCKED: {
      message: 'Job is locked',
      code: 1
    },
    WRONG_FORMAT: {
      message: 'Wrong format @type {{ jobUUID: string }} or missing UUID from the channel',
      code: 2
    }
  }
}