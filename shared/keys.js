'use strict';

const NAMESPACES = Object.freeze({
  CLUSTER: 'CLUSTER'
});

const TYPES = Object.freeze({
  JOBS: 'JOBS'
});

const CLUSTER_MANAGER_PROPERTIES = Object.freeze({
  REQUEST: 'REQUEST',
  RESPONSE: 'RESPONSE'
});

const CLUSTER_CLIENT_PROPERTIES = Object.freeze({
});

module.exports = Object.freeze({
  CLUSTER_CLIENT_PROPERTIES,
  CLUSTER_MANAGER_PROPERTIES,
  NAMESPACES,
  TYPES
});