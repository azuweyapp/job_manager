module.exports = {
  apps : [{
    name: 'cluster_manager',
    script: 'cluster_manager/src/index.js',
    watch: process.env.NODE_ENV !== 'production'
  }]
}