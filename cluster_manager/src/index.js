'use strict';
const RedisModule = require('./modules/redis');

((() => {
  RedisModule.subscribe();
})());

process.on('SIGINT', () => {
  console.info('SIGINT signal received.');
  process.exit(0);
});
