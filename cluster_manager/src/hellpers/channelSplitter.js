/**
 * @param {string} channelName
 * @returns {{ serviceName: string, listName: string, requestType: string, clientUUID: string }}
 */
module.exports = (channelName) => {
  /**
   * @type {Array<string>}
   */
  const partialChannel = channelName.split('::');

  const serviceName = partialChannel[0];
  const listName = partialChannel[1];
  const requestType = partialChannel[2];
  const clientUUID = partialChannel[3];

  return {
    serviceName,
    listName,
    requestType,
    clientUUID
  };
};
