'use strict';

module.exports = ((() => {
  const temporaryStore = {};

  return {
    setItem: (index, item) => {
      temporaryStore[index] = item;
    },
    getItem: (index) => temporaryStore[index]
  };
})());
