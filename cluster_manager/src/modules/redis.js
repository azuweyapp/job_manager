'use strict';
const Redis = require('redis');
const Keys = require('../../../shared/keys');
const SubscribeHandler = require('../handlers/subscribe');

const RedisModule = ((() => {
  const subscriber = Redis.createClient({
    host: 'redishost'
  });

  const publisher = Redis.createClient({
    host: 'redishost'
  });

  return {
    subscribe: () => {
      subscriber.psubscribe(`*::${Keys.TYPES.JOBS}::${Keys.CLUSTER_MANAGER_PROPERTIES.REQUEST}::*`, SubscribeHandler);
      subscriber.on('pmessage', require('../handlers/pMessage'));
    },
    publish: (channel, message, callback) => {
      publisher.publish(channel, message, callback);
    }
  };
})());

module.exports = RedisModule;
