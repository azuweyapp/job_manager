'use strict';
const Errors = require('../../../shared/errors');
const TemporaryStore = require('../stores/temporaryStore');

/**
 * @param {String} serviceName Name of the service
 * @param {String} listName Name of the list
 * @param {String} clientUUID Unique ID of the client
 * @param {{ [x: string]: any, jobUUID: string }} parsedMessage It contains the unique ID of a job
 * @returns { Promise<{[x: string]: any, jobUUID: string, clientUUID: string}> }
 */
module.exports = (serviceName, listName, clientUUID, parsedMessage) => new Promise((resolve, reject) => {
  const jobUUID = parsedMessage.jobUUID;
  if (!jobUUID || jobUUID === '' || clientUUID === '') {
    // Check format
    reject(new Error(Errors.JOB.WRONG_FORMAT));
  } else if (TemporaryStore.getItem([`${serviceName}::${listName}::${jobUUID}`])) {
    // Check job is locked or not
    reject(new Error(Errors.JOB.LOCKED));
  } else {
    // Lock our job
    TemporaryStore.setItem(`${serviceName}::${listName}::${jobUUID}`, clientUUID);
    resolve({
      ...parsedMessage,
      clientUUID: TemporaryStore.getItem([`${serviceName}::${listName}::${jobUUID}`])
    });
  }
});
