const ChannelNameSplitter = require('../hellpers/channelSplitter');
const Keys = require('../../../shared/keys');
const RedisModule = require('../modules/redis');
const RequestHandler = require('./request');

module.exports = (pattern, channel, message) => {
  const {
    serviceName,
    listName,
    requestType,
    clientUUID
  } = ChannelNameSplitter(channel);

  if (listName === Keys.TYPES.JOBS && requestType === Keys.CLUSTER_MANAGER_PROPERTIES.REQUEST) {
    /**
     * @type {{ [x: string]: any, jobUUID: string }}
     */
    const parsedMessage = JSON.parse(message);
    const responseChannel = `${serviceName}::${listName}::${Keys.CLUSTER_MANAGER_PROPERTIES.RESPONSE}::${clientUUID}`;

    RequestHandler(serviceName, listName, clientUUID, parsedMessage).then(response => {
      RedisModule.publish(responseChannel, JSON.stringify(response));
    }).catch(reason => {
      RedisModule.publish(responseChannel, JSON.stringify({
        error: reason.message
      }));
    });
  }
};